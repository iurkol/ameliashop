#!-*-coding:utf-8-*-
from django.contrib import admin
from calls.models import Call

class CallAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone_number', 'text', 'date', 'processed')
    
admin.site.register(Call, CallAdmin)