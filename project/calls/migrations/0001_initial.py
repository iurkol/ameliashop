# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Call'
        db.create_table('calls', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=94)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=24)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=94)),
            ('processed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('calls', ['Call'])


    def backwards(self, orm):
        # Deleting model 'Call'
        db.delete_table('calls')


    models = {
        'calls.call': {
            'Meta': {'object_name': 'Call', 'db_table': "'calls'"},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '94'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '94'})
        }
    }

    complete_apps = ['calls']