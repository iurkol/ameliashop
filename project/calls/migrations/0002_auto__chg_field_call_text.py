# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Call.text'
        db.alter_column('calls', 'text', self.gf('django.db.models.fields.TextField')(max_length=94))

    def backwards(self, orm):

        # Changing field 'Call.text'
        db.alter_column('calls', 'text', self.gf('django.db.models.fields.CharField')(max_length=94))

    models = {
        'calls.call': {
            'Meta': {'object_name': 'Call', 'db_table': "'calls'"},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '94'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '94'})
        }
    }

    complete_apps = ['calls']