#-*-coding: utf-8-*-
from django.db import models
import datetime

class Call(models.Model):
    name = models.CharField(max_length = 94, verbose_name = u"Имя", unique = False)
    phone_number = models.CharField(max_length = 24, verbose_name = u"Номер телефона", unique = False)
    text = models.TextField(max_length = 94, verbose_name = u"Сообщение", unique = False)
    processed = models.BooleanField(blank = True, default = False, verbose_name = u"Обработан")
    comments = models.TextField(verbose_name = u"Примечания", unique = False, blank = True)
    date =  models.DateTimeField(verbose_name = u"Отправлен", auto_now_add = True)
    
    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'calls'
        verbose_name = u'Звонок'
        verbose_name_plural = u'Звонки'
