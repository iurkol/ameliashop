##!-*-coding:utf-8-*-
#from django.contrib import admin
#from mptt.admin import MPTTModelAdmin, FeinCMSModelAdmin
#from models import Cart
#from feincms.admin import tree_editor
#
## This removes 'View on site' buttons in admin page
##del Category.get_absolute_url
##del Product.get_absolute_url
#            
#class CartAdmin(admin.ModelAdmin):
#    list_display = ('cart_id', 'date_added', 'total')
#    
#    class Media:
#        js = [
#           
#           '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
#           '/static/grappelli/tinymce_setup/tinymce_setup.js',
#        ]
#        
#        
#admin.site.register(Cart, CartAdmin)