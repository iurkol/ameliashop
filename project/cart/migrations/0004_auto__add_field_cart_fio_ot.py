# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Cart.fio_ot'
        db.add_column('cart.items', 'fio_ot',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Cart.fio_ot'
        db.delete_column('cart.items', 'fio_ot')


    models = {
        'cart.cart': {
            'Meta': {'ordering': "['date_added']", 'object_name': 'Cart', 'db_table': "'cart.items'"},
            'address_p': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'anon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ask_address': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'bouquet': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Bouquet']"}),
            'bouquet_quantity': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'call_after_delivery': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cart_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'city_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'customer_notification': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'delivery': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'dop_tel_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'dop_tel_p': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fio_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fio_p': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'flower': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Flower']"}),
            'flower_quantity': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_hands': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'make_photo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pay_method': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'postcard_sign': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'postcard_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'pred_zvon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sms_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tel_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tel_p': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'catalog.bouquet': {
            'Meta': {'object_name': 'Bouquet', 'db_table': "'bouquets'"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Category']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_first': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_fourth': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_second': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_third': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_recommended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'promoblock': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sims': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'sims_rel_+'", 'null': 'True', 'to': "orm['catalog.Bouquet']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'catalog.category': {
            'Meta': {'object_name': 'Category', 'db_table': "'categories'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['catalog.Category']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'catalog.flower': {
            'Meta': {'object_name': 'Flower', 'db_table': "'flowers'"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Category']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_first': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_fourth': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_second': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_third': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_recommended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'promoblock': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sims': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'sims_rel_+'", 'null': 'True', 'to': "orm['catalog.Flower']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cart']