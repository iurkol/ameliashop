# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Cart'
        db.delete_table('cart.items')

        # Removing M2M table for field bouquet on 'Cart'
        db.delete_table('cart.items_bouquet')

        # Removing M2M table for field flower on 'Cart'
        db.delete_table('cart.items_flower')

        # Adding model 'CartItem'
        db.create_table('cart.items', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bouquet_quantity', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('flower_quantity', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('bouquet', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Bouquet'], null=True, blank=True)),
            ('flower', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Flower'], null=True, blank=True)),
        ))
        db.send_create_signal('cart', ['CartItem'])


    def backwards(self, orm):
        # Adding model 'Cart'
        db.create_table('cart.items', (
            ('comment', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('city_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('customer_notification', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('make_photo', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('fio_p', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bouquet_quantity', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('ask_address', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cart_id', self.gf('django.db.models.fields.CharField')(max_length=50, unique=True)),
            ('flower_quantity', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('dop_tel_p', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('address_p', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('sms_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('pred_zvon', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('delivery', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('dop_tel_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('fio_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('tel_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('postcard_sign', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('tel_p', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('anon', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('call_after_delivery', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('in_hands', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('pay_method', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('postcard_text', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('cart', ['Cart'])

        # Adding M2M table for field bouquet on 'Cart'
        db.create_table('cart.items_bouquet', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cart', models.ForeignKey(orm['cart.cart'], null=False)),
            ('bouquet', models.ForeignKey(orm['catalog.bouquet'], null=False))
        ))
        db.create_unique('cart.items_bouquet', ['cart_id', 'bouquet_id'])

        # Adding M2M table for field flower on 'Cart'
        db.create_table('cart.items_flower', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cart', models.ForeignKey(orm['cart.cart'], null=False)),
            ('flower', models.ForeignKey(orm['catalog.flower'], null=False))
        ))
        db.create_unique('cart.items_flower', ['cart_id', 'flower_id'])

        # Deleting model 'CartItem'
        db.delete_table('cart.items')


    models = {
        'cart.cartitem': {
            'Meta': {'object_name': 'CartItem', 'db_table': "'cart.items'"},
            'bouquet': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Bouquet']", 'null': 'True', 'blank': 'True'}),
            'bouquet_quantity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'flower': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Flower']", 'null': 'True', 'blank': 'True'}),
            'flower_quantity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'catalog.bouquet': {
            'Meta': {'object_name': 'Bouquet', 'db_table': "'bouquets'"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Category']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_first': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_fourth': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_second': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_third': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_recommended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'promoblock': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sims': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'sims_rel_+'", 'null': 'True', 'to': "orm['catalog.Bouquet']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'catalog.category': {
            'Meta': {'object_name': 'Category', 'db_table': "'categories'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['catalog.Category']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'catalog.flower': {
            'Meta': {'object_name': 'Flower', 'db_table': "'flowers'"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Category']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_first': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_fourth': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_second': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_third': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_recommended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'promoblock': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sims': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'sims_rel_+'", 'null': 'True', 'to': "orm['catalog.Flower']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cart']