#!-*-coding:utf-8-*-
#This is cart models
from django.db import models
from catalog.models import Bouquet, Flower
from django.contrib.auth.models import User

#class Cart(models.Model):
#    cart_id = models.CharField(max_length = 50, verbose_name = u"ID заказа", unique = True)
#    date_added = models.DateTimeField(auto_now_add = True)
#    bouquet_quantity = models.IntegerField(default = 0)
#    flower_quantity = models.IntegerField(default = 0)
#    bouquet = models.ManyToManyField(Bouquet, unique = False, blank = True, null = True)
#    flower = models.ManyToManyField(Flower, unique = False, blank = True, null = True)
#    fio_ot = models.CharField(max_length = 50, verbose_name = u"ФИО отправителя", blank = True)
#    city_ot = models.CharField(max_length = 50, verbose_name = u"Город нахождения", blank = True)
#    tel_ot = models.CharField(max_length = 50, verbose_name = u"Телефон отправителя", blank = True)
#    dop_tel_ot = models.CharField(max_length = 50, verbose_name = u"Дополнительный телефон отправителя", blank = True)
#    sms_ot = models.CharField(max_length = 50, verbose_name = u"Телефон для SMS", blank = True)
#    email = models.CharField(max_length = 50, verbose_name = u"E-mail", blank = True)
#    delivery = models.CharField(max_length = 50, verbose_name = u"Доставка", blank = True)
#    
#    fio_p = models.CharField(max_length = 50, verbose_name = u"ФИО получателя", blank = True)
#    tel_p = models.CharField(max_length = 50, verbose_name = u"Телефон получателя", blank = True)
#    dop_tel_p = models.CharField(max_length = 50, verbose_name = u"Дополнительный телефон получателя", blank = True)
#    address_p = models.CharField(max_length = 50, verbose_name = u"Адрес получателя", blank = True)
#    ask_address = models.BooleanField(verbose_name = u"Уточнить адрес у получателя", default = False)
#    comment = models.TextField(verbose_name = u"Комментарий", blank = True)
#    pred_zvon = models.BooleanField(verbose_name = u"Предварительный звонок", default = False)
#    anon = models.BooleanField(verbose_name = u"Анонимная посылка", default = False)
#    call_after_delivery = models.BooleanField(verbose_name = u"Позвонить отправителю после доставки", default = False)
#    in_hands = models.BooleanField(verbose_name = u"Лично в руки", default = False)
#    make_photo = models.BooleanField(verbose_name = u"Сфотографировать получателя", default = False)
#    pay_method = models.CharField(max_length = 50, verbose_name = u"Метод оплаты", blank = True)
#    postcard_text = models.TextField(verbose_name = u"Текст открытки", blank = True)
#    postcard_sign = models.TextField(verbose_name = u"Подпись", blank = True)
#    
#    # SMS or email
#    customer_notification = models.CharField(max_length = 50, verbose_name = u"Уведомить заказчика о доставке", blank = True)
#    
#    
#    class Meta:
#        db_table = 'cart.items'
#        ordering = ['date_added']
#    
#    # AttributeError: 'NoneType' object has no attribute 'price'    
#    def total(self):
#        #Общая стоимость
#        if self.bouquet_quantity and self.flower_quantity:
#            return str((self.bouquet_quantity * self.bouquet.price) + (self.flower_quantity * self.flower.price)) #Если есть и цветы и букеты
#        elif not self.bouquet_quantity and not self.flower_quantity: #Если нет ни цветов ни букетов
#            return 0
#        elif self.bouquet_quantity and not self.flower_quantity: #Если есть букеты
#            return str((self.bouquet_quantity * self.bouquet.price))
#        else: #Если есть цветы
#            return str((self.flower_quantity * self.flower_price))

class CartItem(models.Model):
    # Это абстрактная сущность одного элемента корзины, который
    # может быть представлен либо букетом, либо цветком. Также определяется количество
    # этих отдельных букетов или цветков в экземляре элемента корзины и их стоимость   
    bouquet_quantity = models.IntegerField(default = 0)
    flower_quantity = models.IntegerField(default = 0)
    bouquet = models.ForeignKey(Bouquet, unique = False, blank = True, null = True)
    flower = models.ForeignKey(Flower, unique = False, blank = True, null = True)
    
    def total(self):
        #Общая стоимость
        if self.bouquet_quantity and self.flower_quantity:
            return str((self.bouquet_quantity * self.bouquet.price) + (self.flower_quantity * self.flower.price)) #Если есть и цветы и букеты
        elif not self.bouquet_quantity and not self.flower_quantity: #Если нет ни цветов ни букетов
            return 0
        elif self.bouquet_quantity and not self.flower_quantity: #Если есть букеты
            return str((self.bouquet_quantity * self.bouquet.price))
        else: #Если есть цветы
            return str((self.flower_quantity * self.flower_price))
        
    
    #def name(self):
    #    if self.bouquet:
    #        return self.bouquet.name
    #    elif self.flower:
    #        return self.flower.name
    
    class Meta:
        db_table = 'cart.items'
        
        
        
#TODO: cart.get_absolute_url   