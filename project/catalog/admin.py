#!-*-coding:utf-8-*-
from django.contrib import admin
from mptt.admin import MPTTModelAdmin, FeinCMSModelAdmin
from models import Category, Flower, Bouquet, Item, FlowerImage, BouquetImage
from feincms.admin import tree_editor

# This removes 'View on site' buttons in admin page
#del Category.get_absolute_url
#del Product.get_absolute_url

class FlowerImageInline(admin.StackedInline):
    model = FlowerImage
    extra = 0
    
class BouquetImageInline(admin.StackedInline):
    model = BouquetImage
    extra = 0


class FlowerInline(admin.StackedInline):
    model = Flower
    # exclude = ('parent',)
    extra = 0
    
class BouquetInline(admin.StackedInline):
    model = Bouquet
   # exclude = ('parent',)
    extra = 0
    
class ItemInline(admin.StackedInline):
    model = Item
   # exclude = ('parent',)
    extra = 0

class CategoryAdmin(FeinCMSModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'title',  'created_at', 'updated_at', 'slug',)
    search_fields = ('name',)
    mptt_level_indent = 25
    inlines = [FlowerInline, BouquetInline]
    
        
class FlowerAdmin(admin.ModelAdmin):
    fields = ('name', 'price', 'is_active', 'is_new', 'is_recommended', 'vip', 'description', 'promoblock', 'slug',  'sims', 'title', 'meta_description', 'meta_keywords', 'category', 'business_bouquet', 'birthday', 'anniversary', 'love', 'interior', 'congrat', 'sorry', 'child', 'holiday_roses', 'wedding', 'thanks', 'horoscope', 'he', 'she',)
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'price', 'is_active', 'is_new', 'is_recommended', 'description', 'category', 'slug',)
    filter_horizontal = ('sims',)
    search_fields = ('name',)
    inlines = [FlowerImageInline,]
    
    class Media:
        js = [
           
           '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
           '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
    
    
class BouquetAdmin(admin.ModelAdmin):
    fields = ('name', 'price', 'is_active', 'is_new', 'is_recommended', 'vip', 'description', 'promoblock', 'slug',  'sims', 'title', 'meta_description', 'meta_keywords', 'category', 'business_bouquet', 'birthday', 'anniversary', 'love', 'interior', 'congrat', 'sorry', 'child', 'holiday_roses', 'wedding', 'thanks', 'horoscope', 'he', 'she',)
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'price', 'is_active', 'is_new', 'is_recommended', 'description', 'category', 'slug',)
    filter_horizontal = ('sims',)
    search_fields = ('name',)
    inlines = [ItemInline, BouquetImageInline]
    
    class Media:
        js = [
           
            
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
   
    
    
        
        
admin.site.register(Category, CategoryAdmin)
admin.site.register(Flower, FlowerAdmin)
admin.site.register(Bouquet, BouquetAdmin)