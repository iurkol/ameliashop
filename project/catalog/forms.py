#!/usr/bin/env python
#!-*-coding:utf8-*-
from django import forms
from project.settings import paginator_value

class CategoryPaginationInputForm(forms.Form):
    value = forms.IntegerField(initial=paginator_value)