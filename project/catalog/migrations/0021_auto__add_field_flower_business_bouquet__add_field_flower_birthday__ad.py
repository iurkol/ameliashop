# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Flower.business_bouquet'
        db.add_column('flowers', 'business_bouquet',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.birthday'
        db.add_column('flowers', 'birthday',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.anniversary'
        db.add_column('flowers', 'anniversary',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.love'
        db.add_column('flowers', 'love',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.interior'
        db.add_column('flowers', 'interior',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.congrat'
        db.add_column('flowers', 'congrat',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.sorry'
        db.add_column('flowers', 'sorry',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.child'
        db.add_column('flowers', 'child',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.holiday_roses'
        db.add_column('flowers', 'holiday_roses',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.wedding'
        db.add_column('flowers', 'wedding',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.thanks'
        db.add_column('flowers', 'thanks',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.horoscope'
        db.add_column('flowers', 'horoscope',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.he'
        db.add_column('flowers', 'he',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Flower.she'
        db.add_column('flowers', 'she',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.business_bouquet'
        db.add_column('bouquets', 'business_bouquet',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.birthday'
        db.add_column('bouquets', 'birthday',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.anniversary'
        db.add_column('bouquets', 'anniversary',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.love'
        db.add_column('bouquets', 'love',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.interior'
        db.add_column('bouquets', 'interior',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.congrat'
        db.add_column('bouquets', 'congrat',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.sorry'
        db.add_column('bouquets', 'sorry',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.child'
        db.add_column('bouquets', 'child',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.holiday_roses'
        db.add_column('bouquets', 'holiday_roses',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.wedding'
        db.add_column('bouquets', 'wedding',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.thanks'
        db.add_column('bouquets', 'thanks',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.horoscope'
        db.add_column('bouquets', 'horoscope',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.he'
        db.add_column('bouquets', 'he',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Bouquet.she'
        db.add_column('bouquets', 'she',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Flower.business_bouquet'
        db.delete_column('flowers', 'business_bouquet')

        # Deleting field 'Flower.birthday'
        db.delete_column('flowers', 'birthday')

        # Deleting field 'Flower.anniversary'
        db.delete_column('flowers', 'anniversary')

        # Deleting field 'Flower.love'
        db.delete_column('flowers', 'love')

        # Deleting field 'Flower.interior'
        db.delete_column('flowers', 'interior')

        # Deleting field 'Flower.congrat'
        db.delete_column('flowers', 'congrat')

        # Deleting field 'Flower.sorry'
        db.delete_column('flowers', 'sorry')

        # Deleting field 'Flower.child'
        db.delete_column('flowers', 'child')

        # Deleting field 'Flower.holiday_roses'
        db.delete_column('flowers', 'holiday_roses')

        # Deleting field 'Flower.wedding'
        db.delete_column('flowers', 'wedding')

        # Deleting field 'Flower.thanks'
        db.delete_column('flowers', 'thanks')

        # Deleting field 'Flower.horoscope'
        db.delete_column('flowers', 'horoscope')

        # Deleting field 'Flower.he'
        db.delete_column('flowers', 'he')

        # Deleting field 'Flower.she'
        db.delete_column('flowers', 'she')

        # Deleting field 'Bouquet.business_bouquet'
        db.delete_column('bouquets', 'business_bouquet')

        # Deleting field 'Bouquet.birthday'
        db.delete_column('bouquets', 'birthday')

        # Deleting field 'Bouquet.anniversary'
        db.delete_column('bouquets', 'anniversary')

        # Deleting field 'Bouquet.love'
        db.delete_column('bouquets', 'love')

        # Deleting field 'Bouquet.interior'
        db.delete_column('bouquets', 'interior')

        # Deleting field 'Bouquet.congrat'
        db.delete_column('bouquets', 'congrat')

        # Deleting field 'Bouquet.sorry'
        db.delete_column('bouquets', 'sorry')

        # Deleting field 'Bouquet.child'
        db.delete_column('bouquets', 'child')

        # Deleting field 'Bouquet.holiday_roses'
        db.delete_column('bouquets', 'holiday_roses')

        # Deleting field 'Bouquet.wedding'
        db.delete_column('bouquets', 'wedding')

        # Deleting field 'Bouquet.thanks'
        db.delete_column('bouquets', 'thanks')

        # Deleting field 'Bouquet.horoscope'
        db.delete_column('bouquets', 'horoscope')

        # Deleting field 'Bouquet.he'
        db.delete_column('bouquets', 'he')

        # Deleting field 'Bouquet.she'
        db.delete_column('bouquets', 'she')


    models = {
        'catalog.bouquet': {
            'Meta': {'object_name': 'Bouquet', 'db_table': "'bouquets'"},
            'anniversary': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'birthday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'business_bouquet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Category']"}),
            'child': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'congrat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'he': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'holiday_roses': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'horoscope': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interior': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_recommended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'love': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'promoblock': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'she': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sims': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'sims_rel_+'", 'null': 'True', 'to': "orm['catalog.Bouquet']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'sorry': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thanks': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'wedding': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'catalog.bouquetimage': {
            'Meta': {'object_name': 'BouquetImage'},
            'bouquet': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Bouquet']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'catalog.category': {
            'Meta': {'object_name': 'Category', 'db_table': "'categories'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['catalog.Category']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'catalog.flower': {
            'Meta': {'object_name': 'Flower', 'db_table': "'flowers'"},
            'anniversary': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'birthday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'business_bouquet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Category']"}),
            'child': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'congrat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'he': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'holiday_roses': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'horoscope': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interior': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_recommended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'love': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'promoblock': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'she': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sims': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'sims_rel_+'", 'null': 'True', 'to': "orm['catalog.Flower']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'sorry': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thanks': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'wedding': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'catalog.flowerimage': {
            'Meta': {'object_name': 'FlowerImage'},
            'flower': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Flower']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'catalog.item': {
            'Meta': {'object_name': 'Item', 'db_table': "'items'"},
            'bouq': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Bouquet']"}),
            'flower': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Flower']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'max_length': '3'})
        }
    }

    complete_apps = ['catalog']