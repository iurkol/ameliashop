#!-*-coding:utf-8-*-
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
import mptt

#def count_products(category):
#    '''Подсчет количества позиций в категории и ее подкатегориях'''
#    prod_num = category.product_set.count()
#    for s in category.children.all():
#        prod_num += count_products(s)
#    return prod_num


class Category(MPTTModel):
    name = models.CharField(max_length=100, unique=True, verbose_name = u'Название категории')
    created_at = models.DateTimeField(auto_now_add = True, verbose_name = u'Дата добавления')
    updated_at =  models.DateTimeField(auto_now = True, verbose_name = u'Дата изменения')
    meta_description = models.CharField(max_length = 255, blank = True, verbose_name = u"Содержимое тега meta description для страницы категории(необязательно)")
    meta_keywords = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta kewords для страницы категории(необязательно)')
    title = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега title для страницы категории(необязательно)')
    slug = models.SlugField(max_length = 50, unique = True, verbose_name = u'Уникальный URL страницы категории')
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', verbose_name = u'Родительская категория')
    
    def __unicode__(self):
        return self.name
    
    def get_absolute_url(self):
        return (self.slug)
    
    def prod_nr(self):
        return count_products(self)
    
    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        db_table = 'categories'
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'
        #ordering = ['tree_id', 'lft']


       
        
class Flower(models.Model):
    name = models.CharField(max_length = 255, verbose_name = u'Название цветка')
    sims = models.ManyToManyField('self', verbose_name = u'Похожие цветы', blank = True, null = True)
    price = models.IntegerField(null = True, blank = True, verbose_name = u'Цена')
    is_new = models.BooleanField(default = False, verbose_name = u'Новинка')
    is_recommended = models.BooleanField(default = False, verbose_name = u'Рекомендовано')
    is_active = models.BooleanField(default = False, verbose_name = u'Включено')
    description = models.TextField(verbose_name = u'Текстовое описание')
    category = models.ForeignKey(Category, verbose_name = u'Категория')
    created_at = models.DateTimeField(auto_now_add = True, verbose_name = u'Дата добавления')
    updated_at =  models.DateTimeField(auto_now = True, verbose_name = u'Дата изменения')
    meta_description = models.CharField(max_length = 255, blank = True, verbose_name = u"Содержимое тега meta description для страницы цветка(необязательно)")
    meta_keywords = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta kewords для страницы цветка(необязательно)')
    title = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега title для страницы цветка(необязательно)')
    slug = models.SlugField(max_length = 50, unique = True, verbose_name = u'Уникальный URL страницы цветка')
    #image_first = models.ImageField(upload_to = 'flower_image', verbose_name = u'Изображение 1')
    #image_second = models.ImageField(upload_to = 'flower_image', verbose_name = u'Изображение 2')
    #image_third = models.ImageField(upload_to = 'flower_image', verbose_name = u'Изображение 3')
    #image_fourth = models.ImageField(upload_to = 'flower_image', verbose_name = u'Изображение 4')
    promoblock = models.TextField(verbose_name = u'Контент', blank = True)
    vip = models.BooleanField(default = False, verbose_name = u'VIP')
    
    #reason
    business_bouquet = models.BooleanField(default = False, verbose_name = u'Бизнес букет')
    birthday = models.BooleanField(default = False, verbose_name = u'День рождения')
    anniversary = models.BooleanField(default = False, verbose_name = u'Юбилей')
    love = models.BooleanField(default = False, verbose_name = u'Цветы любви')
    interior = models.BooleanField(default = False, verbose_name = u'Оформление интерьера')
    congrat = models.BooleanField(default = False, verbose_name = u'Поздравляю')
    sorry = models.BooleanField(default = False, verbose_name = u'Прости')
    child = models.BooleanField(default = False, verbose_name = u'Рождение ребенка')
    holiday_roses = models.BooleanField(default = False, verbose_name = u'Розы на праздник')
    wedding = models.BooleanField(default = False, verbose_name = u'Свадьба')
    thanks = models.BooleanField(default = False, verbose_name = u'Спасибо')
    horoscope = models.BooleanField(default = False, verbose_name = u'Цветы по гороскопу')
    
    #receiver
    he = models.BooleanField(default = False, verbose_name = u'Получатель: Он')
    she = models.BooleanField(default = False, verbose_name = u'Получатель: Она')
    
    
    
    
    def get_first_image(self):
        try:
            return self.flowerimage_set.all()[0]
        except IndexError:
            return False
    def get_all_images(self):
        try:
            return self.flowerimage_set.all()
        except:
            return False
    
    def __unicode__(self):
        return self.name
    
    def get_absolute_url(self):
        return (self.slug)
    
    class Meta:
        db_table = 'flowers'
        verbose_name = u'Цветок'
        verbose_name_plural = u'Цветы'
        
        
class FlowerImage(models.Model):
    flower = models.ForeignKey(Flower)
    image = models.ImageField(upload_to = 'flower_image', verbose_name = u'Изображение цветка')
    
    def __unicode__(self):
        return self.image.url
    
    class Meta:
        verbose_name = u'Изображение цветка'
        verbose_name_plural = u'Изображения цветка'   




class Bouquet(models.Model):
    name = models.CharField(max_length = 255, verbose_name = u'Название букета')
    sims = models.ManyToManyField('self', verbose_name = u'Похожие букеты', blank = True, null = True)
    price = models.IntegerField(null = True, blank = True, verbose_name = u'Цена')
    is_new = models.BooleanField(default = False, verbose_name = u'Новинка')
    is_recommended = models.BooleanField(default = False, verbose_name = u'Рекомендовано')
    is_active = models.BooleanField(default = False, verbose_name = u'Включено')
    description = models.TextField(verbose_name = u'Текстовое описание')
    category = models.ForeignKey(Category, verbose_name = u'Категория')
    created_at = models.DateTimeField(auto_now_add = True, verbose_name = u'Дата добавления')
    updated_at =  models.DateTimeField(auto_now = True, verbose_name = u'Дата изменения')
    meta_description = models.CharField(max_length = 255, blank = True, verbose_name = u"Содержимое тега meta description для страницы букета(необязательно)")
    meta_keywords = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta kewords для страницы букета(необязательно)')
    title = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега title для страницы букета(необязательно)')
    slug = models.SlugField(max_length = 50, unique = True, verbose_name = u'Уникальный URL страницы букета')
    #image_first = models.ImageField(upload_to = 'bouquete_image', verbose_name = u'Изображение 1')
    #image_second = models.ImageField(upload_to = 'bouquete_image', verbose_name = u'Изображение 2')
    #image_third = models.ImageField(upload_to = 'bouquete_image', verbose_name = u'Изображение 3')
    #image_fourth = models.ImageField(upload_to = 'bouquete_image', verbose_name = u'Изображение 4')
    promoblock = models.TextField(verbose_name = u'Контент', blank = True)
    vip = models.BooleanField(default = False, verbose_name = u'VIP')
    
    #reason
    business_bouquet = models.BooleanField(default = False, verbose_name = u'Бизнес букет')
    birthday = models.BooleanField(default = False, verbose_name = u'День рождения')
    anniversary = models.BooleanField(default = False, verbose_name = u'Юбилей')
    love = models.BooleanField(default = False, verbose_name = u'Цветы любви')
    interior = models.BooleanField(default = False, verbose_name = u'Оформление интерьера')
    congrat = models.BooleanField(default = False, verbose_name = u'Поздравляю')
    sorry = models.BooleanField(default = False, verbose_name = u'Прости')
    child = models.BooleanField(default = False, verbose_name = u'Рождение ребенка')
    holiday_roses = models.BooleanField(default = False, verbose_name = u'Розы на праздник')
    wedding = models.BooleanField(default = False, verbose_name = u'Свадьба')
    thanks = models.BooleanField(default = False, verbose_name = u'Спасибо')
    horoscope = models.BooleanField(default = False, verbose_name = u'Цветы по гороскопу')
    
    #receiver
    he = models.BooleanField(default = False, verbose_name = u'Получатель: Он')
    she = models.BooleanField(default = False, verbose_name = u'Получатель: Она')
    
    def get_first_image(self):
        try:
            return self.bouquetimage_set.all()[0]
        except IndexError:
            return False
    def get_all_images(self):
        try:
            return self.bouquetimage_set.all()
        except:
            return False
    
    def __unicode__(self):
        return self.name
    
    def get_absolute_url(self):
        return (self.slug)
    
    class Meta:
        db_table = 'bouquets'
        verbose_name = u'Букет'
        verbose_name_plural = u'Букеты'
        
class BouquetImage(models.Model):
    bouquet = models.ForeignKey(Bouquet)
    image = models.ImageField(upload_to = 'flower_image', verbose_name = u'Изображение букета') 
    
    def __unicode__(self):
        return self.image.url
    
    class Meta:
        verbose_name = u'Изображение букета'
        verbose_name_plural = u'Изображения букета'
        

class Item(models.Model):
    #"""Модель для элемента букета, то есть цветка"""
    flower = models.ForeignKey(Flower, verbose_name = u'Цветок',  blank = True, null = True)
    quantity = models.IntegerField(max_length = 3, verbose_name = u'Количество')
    bouq = models.ForeignKey(Bouquet)
    
    def __unicode__(self):
        return self.flower.name
    
    class Meta:
        db_table = 'items'
        verbose_name = u'Элемент букета'
        verbose_name_plural = u'Элемент букета'








