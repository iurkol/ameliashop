#!/usr/bin/env python
# -*- coding: utf8 -*-

from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator,  EmptyPage, PageNotAnInteger
from itertools import chain
from project.settings import *
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from contman.models import AboutPage, DeliveryPage, DiscountsPage, PaymentPage, IndexPage, SliderItem
from catalog.models import Category, Flower, Bouquet
from catalog.forms import CategoryPaginationInputForm
from calls.models import Call
from news.models import News
from services.models import Services
from django.contrib import auth
from django.contrib.auth.models import User
from order.models import  Order
import uuid
import pytils
#from django.core import serializers
from django.utils import simplejson as json


def test_view(request):
    cart = request.session.get('cart', False)
    order = request.session.get('order')
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
    return render_to_response('test.html', locals())
    #return HttpResponse(order.cart_id)
    
def index_view(request):
    order = request.session.get('order')
    # Картинки и слоганы слайдера промоблока главной страницы
    slider_items = SliderItem.objects.all()
    nodes = Category.objects.all()
    cats = Category.tree.all()
    news = News.objects.filter(public = True)
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
        
    
    try:
        text = IndexPage.objects.all()[0]
        cont = text.content
    except IndexError:
        cont = 'Empty content'
    return render_to_response('index.html', locals())
    #return HttpResponse(slider_items[0].slogan)
    
def about_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    nodes = Category.objects.all()
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    try:
        content = AboutPage.objects.all()[0]
    except IndexError:
        content = 'Empty content'
    return render_to_response('about.html', locals())

def delivery_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    nodes = Category.objects.all()
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    try:
        content = DeliveryPage.objects.all()[0]
    except IndexError:
        content = 'Empty content'
    return render_to_response('delivery.html', locals())

def all_recommendations_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
        
    cats = Category.tree.all()
    nodes = Category.objects.all()
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))    
    return render_to_response('all_recomms.html', locals())

def all_new_prod_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    nodes = Category.objects.all()
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    return render_to_response('all_new_prods.html', locals())

def show_sims_view(request, product_slug):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    nodes = Category.objects.all()
    cats = Category.tree.all()
    services = Services.objects.filter(public = True)
    news = News.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    try:
        father = Flower.objects.get(slug = product_slug)
        elements = False # У цветка нет элеметов 
    except ObjectDoesNotExist:
        father = Bouquet.objects.get(slug = product_slug)
        elements = father.item_set.all() #Элементы букета
    sims = father.sims.all() #Похожие позиции
    
    return render_to_response('sims.html', locals())

def sorting_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    services = Services.objects.filter(public = True)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    nodes = Category.objects.all()
    cats = Category.tree.all()
    
    
    
    #Границы цены
    price_lo = int(request.GET['tsena'].split()[0])
    price_hi = int(request.GET['tsena'].split()[1])
    
    #Создаем Queryset - цветы или букеты
    if request.GET['flowerBouqueteRadio'] == 'f':
        q = Flower.objects.filter(is_active = True)
    else:
        q = Bouquet.objects.filter(is_active = True)
        
    #Выбираем получателя
    if request.GET.get('poluchatel') == 'he':
        q = q.filter(he = True)
    elif request.GET.get('poluchatel') == 'she':
        q = q.filter(she = True)    
  
    #Выбираем из них VIP
    if request.GET.get("VIP"):
        q = q.filter(vip = True)
        
    #Фильтруем по цене
    q = q.filter(price__gte = price_lo).filter(price__lte = price_hi)
    
    #Фильтруем по поводу
    if not request.GET.get('povod') == 'empty':
        criterion = request.GET.get('povod')
        res = []
        for x in q:
            if getattr(x, criterion) == True:
                res.append(x)
    else:
        res = list(q)
    
    return render_to_response('sort.html', locals())

def payment_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    nodes = Category.objects.all()
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    try:
        content = PaymentPage.objects.all()[0]
    except IndexError:
        content = 'Empty content'
    return render_to_response('payment.html', locals())

def discounts_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    nodes = Category.objects.all()
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    try:
        content = DiscountsPage.objects.all()[0]
    except IndexError:
        content = 'Empty content'
    return render_to_response('discounts.html', locals())

def contacts_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    nodes = Category.objects.all()
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    return render_to_response('contacts.html', locals())



def show_category(request, category_slug):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    nodes = Category.objects.all()
    cats = Category.tree.all()
    services = Services.objects.filter(public = True)
    cat = get_object_or_404(Category, slug = category_slug) #Current category
    
    flowers = cat.flower_set.all().filter(is_active = True) #Flowers of current category
    bouquets = cat.bouquet_set.all().filter(is_active = True) #Bouquets of current category
    item_list = list(chain(flowers, bouquets)) # List of all items: bouquets and flowers
    #List per page
    if request.session.get('lpp') and not request.POST.get('pagin'): #Если в сессии есть этот ключ и запрос не POST:
        category_lpp = request.session['lpp']
    else:
        try:
            request.session['lpp'] = int(request.POST['pagin'])
            category_lpp = request.session['lpp']
        except KeyError:
            request.session['lpp'] = 1
            category_lpp = request.session['lpp']
    
    if category_lpp == 1:
        ca_lpp = 6
    elif category_lpp == 2:
        ca_lpp = 9
    elif category_lpp == 3:
        ca_lpp = 12
    elif category_lpp == 4:
        ca_lpp = 15
    else:
        ca_lpp = len(item_list)
        
    paginator = Paginator(item_list, ca_lpp)
    page = request.GET.get('page')
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        items = paginator.page(1) # items - это страница
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        items = paginator.page(paginator.num_pages)
    return render_to_response('category.html', locals())
    
    
def show_product(request, product_slug):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    nodes = Category.objects.all()
    cats = Category.tree.all()
    services = Services.objects.filter(public = True)
    try:
        prod = Flower.objects.get(slug = product_slug)
        elements = False # У цветка нет элеметов
        is_flower = True
    except ObjectDoesNotExist:
        prod = Bouquet.objects.get(slug = product_slug)
        elements = prod.item_set.all() #Элементы букета
        is_flower = False
    sims = prod.sims.all() #Похожие позиции
    try:
        if cart.get(prod):
            cart_bttn = True
        else:
            cart_bttn = False
    except AttributeError:
        cart_bttn = False
    cat = prod.category
    # Product breadcrumbs
    prodcrumbs = [x for x in cat.get_ancestors()]
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    return render_to_response('product.html', locals())
    #return HttpResponse(prod.get_all_images()[0].image.url)
    
    
def show_new(request, new_slug):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    new = get_object_or_404(News, slug = new_slug)
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    nodes = Category.objects.all()
    return render_to_response('new.html', locals())

def show_service(request, service_slug):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    service = get_object_or_404(Services, slug = service_slug)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    services = Services.objects.filter(public = True)
    nodes = Category.objects.all()    
    return render_to_response('service.html', locals())

def call_view(request):
    order = request.session.get('order')
    if request.is_ajax:
        data = u"Спасибо, мы Вам перезвоним"
        rp = request.POST
        if rp["radio"] == "rightnow" and rp["phone_number"]: 
            message = u"Получен звонок от {0}. Перезвонить прямо сейчас по номеру {1}".format(rp["name"], rp["phone_number"])
            Call.objects.create(name = rp["name"], text = message, phone_number = rp["phone_number"])
            return HttpResponse(data)
        elif rp["radio"] == "hours" and rp["phone_number"]:
            message = u"Получен звонок от {0}. Перезвонить с {1} до {2} по номеру {3}".format(rp["name"], rp["from"], rp["to"], rp["phone_number"])
            Call.objects.create(name = rp["name"], text = message, phone_number = rp["phone_number"])
            
            return HttpResponse(data)
        
def register_view(request):
    if request.is_ajax:
        rp = request.POST
        if rp["login_email"] and rp["login_password"]:
            new_user = User(username = str(rp["login_email"]), email = rp["login_email"])
            try:
                User.objects.get(email = rp["login_email"])
                message = u'Пользователь с таким электронным адресом уже существет!'
            except ObjectDoesNotExist:
                new_user.set_password(rp["login_password"])
                new_user.save()
                message = u"Вы успешно зарегистрированы.\nВойдите в систему, используя Ваши e-mail и пароль."
            return HttpResponse(message)
            #return HttpResponseRedirect('/test')

#def login_view(request):
#    username = request.POST.get('login_name', '')
#    password = request.POST.get('login_password', '')
#    user = auth.authenticate(username = username, password = password)
#    request.user = user
#    cart = request.session.get('cart', False)
#    if user is not None and user.is_active:
#        auth.login(request, user)
#        message = u"Вход успешен"
#        #request.session['cart'] = cart
#    else:
#        message = u"Пользователь с такими данными не существет"
#        #request.session['cart'] = cart
#    return HttpResponse(message)
    #return HttpResponseRedirect(request.path)
    
def login_view(request):
    order = request.session.get('order')
    email = request.POST.get('login_email', '')
    password = request.POST.get('login_password', '')
    try:
        # Пробуем выловить объект пользователя по его мылу 
        u = User.objects.get(email = email)
        user = auth.authenticate(username = u.username, password = password)
        request.user = user
    except:
        user = None
        message = u"Пользователь с такими данными не существет"
    if user is not None and user.is_active:
        auth.login(request, user)
        message = u"Вход успешен"
        #request.session['cart'] = cart
    else:
        message = u"Пользователь с такими данными не существет"
        #request.session['cart'] = cart
    #return HttpResponse(message)
    return HttpResponse(message)


def logout_view(request):
    auth.logout(request)
    return HttpResponseRedirect("/")


def ajax_cart_info_view(request):
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)
            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    else:
        word =  0
        total_summ = 0
        
    jsonDict = { "word": word, "total_summ": total_summ }
    return HttpResponse( json.dumps( jsonDict ), mimetype="application/json" )

def add_to_cart(request, product_slug):
    try:
        prod = Flower.objects.get(slug = product_slug)
    except ObjectDoesNotExist:
        prod = Bouquet.objects.get(slug = product_slug)
    
    if request.session.get('cart', False):
        if prod in request.session['cart']:
            request.session['cart'][prod] += 1
        else:
            (request.session['cart']).update({prod : 1})
    else:
        request.session['cart'] = {}
        (request.session['cart']).update({prod : 1})
    cart = request.session['cart']
    if request.is_ajax():
        return HttpResponse(u"{0} - добавлено".format(prod.name))
    else:
        return HttpResponseRedirect('/product/{0}'.format(prod.get_absolute_url()))


def show_cart_view(request):
    services = Services.objects.filter(public = True)
    order = request.session.get('order')
    cart = request.session.get('cart', False)
    if cart:
        cart_len = 0
        for k,v in cart.items():
            cart_len += int(v)            
        total_summ = 0
        for k,v in cart.items():
            total_summ += (k.price * int(v))
            
        word = pytils.numeral.get_plural(cart_len, u"букет, букета, букетов")
    cats = Category.tree.all()
    nodes = Category.objects.all()
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    return render_to_response('cart.html', locals())

def clear_cart_view(request):
    if request.is_ajax:
        cart = request.session.get('cart', False)
        if cart:
            del request.session['cart']
        if request.session.get('order'):
            del request.session['order']
        return HttpResponseRedirect('/')    



def updatecart_view(request):
    order = request.session.get('order')
    rp = request.POST
    request.session['cart'] = {}
    d = {}
    for k, v in rp.items():
        #if not v:
        #    del k
        try:
           k = Flower.objects.get(name = k.encode('utf-8'))
        except:
           k = Bouquet.objects.get(name = k.encode('utf-8')) 
        d.update({k:int(v)})
    request.session['cart'] = d
    #return HttpResponseRedirect('/cart')
    return HttpResponse(d)

def order_view(request):
    user = request.user
    rp = request.POST
    cart = request.session['cart']
    total_summ = 0
    for k,v in cart.items():
        total_summ += (k.price * int(v))
    order  = Order(cart_id = str(uuid.uuid1()), customer = user)
    order.cart = ''    
    for k, v in cart.items():
        order.cart += (u'\n {0} -- {1} \n'.format(k, v))
    order.total_summ = total_summ
    order.fio_ot = rp.get('proctelname')
    order.tel_ot = rp.get('proctelnumber')
    if rp.get('rightnow'):
        order.comment = u'Быстрое оформление. Перезвонить прямо сейчас.'
        order.save()
    if rp.get('time'):
        order.comment = u'Быстрое оформление. Перезвонить c {0} до {1}'.format(rp.get('proccallfrom'), rp.get('proccallto'))
        order.save()
        del cart
    #return HttpResponse(rp)
    return HttpResponseRedirect('/clear_cart')

def secondstep_view(request):
    user = request.user
    rp = request.POST
    cart = request.session['cart']
    total_summ = 0
    for k,v in cart.items():
        total_summ += (k.price * int(v))
    #order  = Order(cart_id = str(uuid.uuid1()), customer = user)
    try:
        order = request.session['order']
    except KeyError:
        order  = request.session['order'] = Order(cart_id = str(uuid.uuid1()), customer = user)    
    order.cart = ''    
    for k, v in cart.items():
        order.cart += (u'\n {0} -- {1} \n'.format(k, v))
    order.total_summ = total_summ
    order.fio_ot = rp.get('ot_fio')
    try:
        user.first_name = rp.get('ot_fio').split()[0]
        user.last_name = str(rp.get('ot_fio').split()[1])
        user.save()
    except AttributeError:
        pass        
    order.city_ot = rp.get('ot_city')
    order.tel_ot = rp.get('ot_telnumber')
    order.dop_tel_ot = rp.get('ot_dopnumber')
    order.email = rp.get('ot_email')
    if rp.get('sms_notification'):
        order.customer_notification = u'Да'
    order.save()
    return HttpResponse(request.session['order'])

def thirdstep_view(request):
    rp = request.POST
    order = request.session['order']
    order.delivery = u''
    if rp.get('currier'):
        order.delivery += u'Доставка курьером. \n Город доставки -- {0}. Дата доставки -- {1}. '.format(rp.get('currier_city'), rp.get('currier_date'))
    elif rp.get('selfevac'):
        order.delivery += u'Самовывоз. \n Дата самовывоза -- {0}. '.format(rp.get('selfevac_date'))
        
    if rp.get('currier_time_radio'):
        order.delivery += u'Время доставки с {0} до {1}'.format(rp.get('currier_from'), rp.get('currier_to'))
    if rp.get('selfevac_time'):
        order.delivery += u'Время самовывоза с {0} до {1}'.format(rp.get('selfevac_from'), rp.get('selfevac_to'))
    order.save()
    return HttpResponse(request.session['order'])

def fourthstep_view(request):
    rp = request.POST
    order = request.session.get('order')
    cart = request.session.get('cart')
    
    order.fio_p = rp.get('p_fio')
    order.tel_p = rp.get('p_tel')
    order.address_p = rp.get('p_address')
    if rp.get('p_askaddress'):
        order.address_p = u"Уточнить адрес у получателя"
        order.ask_address = True
    order.comment = rp.get('p_comment')
    
    if rp.get('p_pred_zvon'):
        order.pred_zvon = True
    if rp.get('p_anon'):
        order.anon = True
    if rp.get('p_inhands'):
        order.in_hands = True
    if rp.get('p_makephoto'):
        order.make_photo = True
    
    order.save()    
    return HttpResponse(rp)

def fifthstep_view(request):
    rp = request.POST
    order = request.session.get('order')
    cart = request.session.get('cart')
    
    if rp.get('cash_for_courier'):
        order.pay_method = u"Наличными курьеру"
    elif rp.get('cash_in_salon'):
        order.pay_method = u"Наличными в салоне"
    elif rp.get('clearing'):
        order.pay_method = u"Безналичный расчет"
    order.save()
    return HttpResponse(rp)

def sixthstep_view(request):
    rp = request.POST
    order = request.session.get('order')
    cart = request.session.get('cart')
    if rp.get('is_postcard') and rp.get('postcard_text'):
        order.postcard_text = rp['postcard_text']
    if rp.get('is_postcard') and rp.get("postcard_signature"):
        order.postcard_sign = rp["postcard_signature"]
    if rp.get('eula'):
        order.eula = True
    order.save()
    del(request.session['order'])
    del(request.session['cart'])
    return HttpResponse(rp)

def processor_view(request):
    return HttpResponse("OK")


def success_view(request):
    nodes = Category.objects.all()
    cats = Category.tree.all()
    news = News.objects.filter(public = True)
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    return render_to_response("success.html", locals())


def error_view(request):
    nodes = Category.objects.all()
    cats = Category.tree.all()
    news = News.objects.filter(public = True)
    services = Services.objects.filter(public = True)
    new_products = list(chain(Flower.objects.filter(is_active = True).filter(is_new = True), Bouquet.objects.filter(is_active = True).filter(is_new = True)))
    recommended_products =list(chain(Flower.objects.filter(is_active = True).filter(is_recommended = True), Bouquet.objects.filter(is_active = True).filter(is_recommended = True)))
    return render_to_response("error.html", locals())