#!-*-coding:utf-8-*-
from django.contrib import admin
from django.contrib.sites.models import Site
from contman.models import AboutPage, DeliveryPage, DiscountsPage, PaymentPage, SliderItem, IndexPage

     
       
#class AboutPageImageInline(admin.StackedInline):
#    model = AboutPageImage
#    extra = 1
        
class SliderInline(admin.StackedInline):
    model = SliderItem
    extra = 1
    
class IndexPageAdmin(admin.ModelAdmin):
    list_display = ('content','title', 'meta_keywords', 'meta_description',)
    inlines = [SliderInline]
    
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
    
class AboutPageAdmin(admin.ModelAdmin):
    list_display = ('content','title', 'meta_keywords', 'meta_description',)
    
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
        

class DeliveryPageAdmin(admin.ModelAdmin):
    list_display = ('content', 'title', 'meta_keywords', 'meta_description',)
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
        
class DiscountsPageAdmin(admin.ModelAdmin):
    list_display = ('content', 'title', 'meta_keywords', 'meta_description',)
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
        
class PaymentPageAdmin(admin.ModelAdmin):
    list_display = ('content','title', 'meta_keywords', 'meta_description',)
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
    
    
    
    
admin.site.unregister(Site)
admin.site.register(AboutPage, AboutPageAdmin)
admin.site.register(DeliveryPage, DeliveryPageAdmin)
admin.site.register(DiscountsPage, DiscountsPageAdmin)
admin.site.register(PaymentPage, PaymentPageAdmin)
admin.site.register(IndexPage, IndexPageAdmin)