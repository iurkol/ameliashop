#!-*-coding:utf-8-*-
from django.db import models


    
    
class IndexPage(models.Model):
    content = models.TextField(u'Содержимое блока "О нас"')
    meta_description = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta description для  страницы "О нас"')
    meta_keywords = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta kewords для страницы "О нас"')
    title = models.CharField(max_length = 255, blank = False, verbose_name = u'Содержимое тега title для страницы для  страницы "О нас"')
    
    class Meta:
        verbose_name = u'Контент для главной страницы'
        verbose_name_plural = u'Контент для главной страницы '
    
    def __unicode__(self):
        return u'%s' % self.id
    
    
class SliderItem(models.Model):
    image = models.ImageField(upload_to = "slider_items")
    slogan = models.CharField(max_length = 55, blank = True, verbose_name = u'Слоган в промоблоке')
    pageObject = models.ForeignKey(IndexPage)
    link = models.CharField(max_length = 55, blank = True, verbose_name = u'Ссылка в промоблоке')
    
    class Meta:
        verbose_name = u'Элемент слайдера'
        verbose_name_plural = u'Элементы слайдера'
    
    def __unicode__(self):
        return u'%s' % self.id
    

class AboutPage(models.Model):
    content = models.TextField(u'Содержимое')
    meta_description = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta description для  страницы "О нас"')
    meta_keywords = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta kewords для страницы "О нас"')
    title = models.CharField(max_length = 255, blank = False, verbose_name = u'Содержимое тега title для страницы для  страницы "О нас"')
    
    class Meta:
        verbose_name = u'Контент для  страницы "О нас"'
        verbose_name_plural = u'Контент для  страницы "О нас"'
    
    def __unicode__(self):
        return u'%s' % self.id


class DeliveryPage(models.Model):
    content = models.TextField(u'Содержимое')
    meta_description = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta description для  страницы "Доставка"')
    meta_keywords = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta kewords для страницы "Доставка"')
    title = models.CharField(max_length = 255, blank = False, verbose_name = u'Содержимое тега title для страницы для  страницы "Доставка"')
    
    class Meta:
        verbose_name = u'Контент для  страницы "Доставка"'
        verbose_name_plural = u'Контент для  страницы "Доставка"'
    
    def __unicode__(self):
        return u'%s' % self.id
    

class DiscountsPage(models.Model):
    content = models.TextField(u'Содержимое')
    meta_description = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta description для  страницы "Скидки"')
    meta_keywords = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta kewords для страницы "Скидки"')
    title = models.CharField(max_length = 255, blank = False, verbose_name = u'Содержимое тега title для страницы для  страницы "Скидки"')
    
    class Meta:
        verbose_name = u'Контент для  страницы "Скидки"'
        verbose_name_plural = u'Контент для  страницы "Скидки"'
    
    def __unicode__(self):
        return u'%s' % self.id
    
    
class PaymentPage(models.Model):
    content = models.TextField(u'Содержимое')
    meta_description = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta description для  страницы "Оплата"')
    meta_keywords = models.CharField(max_length = 255, blank = True, verbose_name = u'Содержимое тега meta kewords для страницы "Оплата"')
    title = models.CharField(max_length = 255, blank = False, verbose_name = u'Содержимое тега title для страницы для  страницы "Оплата"')
    
    class Meta:
        verbose_name = u'Контент для  страницы "Оплата"'
        verbose_name_plural = u'Контент для  страницы "Оплата"'
    
    def __unicode__(self):
        return u'%s' % self.id
    

