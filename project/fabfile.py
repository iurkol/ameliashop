#!/usr/bin/env python

from fabric.api import *

def sync_db():
    with settings(warn_only=True): 
        local('python manage.py schemamigration catalog --auto')
        local('python manage.py migrate catalog')
        local('python manage.py schemamigration contman --auto')
        local('python manage.py migrate contman')