#!-*-coding:utf-8-*-
from django.contrib import admin
from mptt.admin import MPTTModelAdmin, FeinCMSModelAdmin
from news.models import News


#class NewImageInline(admin.StackedInline):
#    model = NewImage
#    extra = 4

class NewsAdmin(admin.ModelAdmin):
    
    list_display = ('date', 'title', 'public', 'anounce',)
    prepopulated_fields = {'slug' : ('title',)}
    
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
    
admin.site.register(News, NewsAdmin)