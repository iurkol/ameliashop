# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'News'
        db.create_table('news', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.CharField')(max_length=18)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=158)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('public', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('date_add', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('news', ['News'])

        # Adding model 'NewImage'
        db.create_table('new_image', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('new', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['news.News'])),
        ))
        db.send_create_signal('news', ['NewImage'])


    def backwards(self, orm):
        # Deleting model 'News'
        db.delete_table('news')

        # Deleting model 'NewImage'
        db.delete_table('new_image')


    models = {
        'news.newimage': {
            'Meta': {'object_name': 'NewImage', 'db_table': "'new_image'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'new': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['news.News']"})
        },
        'news.news': {
            'Meta': {'ordering': "['-date_add']", 'object_name': 'News', 'db_table': "'news'"},
            'date': ('django.db.models.fields.CharField', [], {'max_length': '18'}),
            'date_add': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '158'})
        }
    }

    complete_apps = ['news']