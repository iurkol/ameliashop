# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'NewImage'
        db.delete_table('new_image')


    def backwards(self, orm):
        # Adding model 'NewImage'
        db.create_table('new_image', (
            ('new', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['news.News'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('news', ['NewImage'])


    models = {
        'news.news': {
            'Meta': {'ordering': "['-date_add']", 'object_name': 'News', 'db_table': "'news'"},
            'date': ('django.db.models.fields.CharField', [], {'max_length': '18'}),
            'date_add': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '158'})
        }
    }

    complete_apps = ['news']