# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'News.anounce'
        db.add_column('news', 'anounce',
                      self.gf('django.db.models.fields.TextField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'News.anounce'
        db.delete_column('news', 'anounce')


    models = {
        'news.news': {
            'Meta': {'ordering': "['-date_add']", 'object_name': 'News', 'db_table': "'news'"},
            'anounce': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '18'}),
            'date_add': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '158'})
        }
    }

    complete_apps = ['news']