#!-*- coding:utf-8 -*-

from django.db import models
    
class News(models.Model):
    date = models.CharField(u'Дата', max_length = 18)
    title = models.CharField(u'Заголовок', max_length = 158)
    anounce = models.TextField(u'Анонс новости на главной странице')
    text = models.TextField(u'Текст')
    public = models.BooleanField(verbose_name = u'Опубликовать')
    slug = models.SlugField(unique = True)
    date_add = models.DateTimeField(auto_now_add = True)
    
    class Meta:
        db_table = 'news'
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'
        ordering = ['-date_add']
        #app_label = u'Новости'
    
    def __unicode__(self):
        return self.title
    
    def get_absolute_url(self):
        return self.slug
    
#class NewImage(models.Model):
#    image = models.ImageField(u'Изображение новости', upload_to = 'news_images')
#    new = models.ForeignKey(News)
#    
#    def __unicode__(self):
#        return u'Изображение № %s'%(self.id)
#    
#    class Meta:
#        db_table = 'new_image'
#        verbose_name = u'Изображение новости'