#!-*-coding:utf-8-*-
from django.contrib import admin
from mptt.admin import MPTTModelAdmin, FeinCMSModelAdmin
from models import Order
from feincms.admin import tree_editor

# This removes 'View on site' buttons in admin page
#del Category.get_absolute_url
#del Product.get_absolute_url
            
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'customer', 'date_added', 'total_summ']
    class Media:
        js = [
           
           '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
           '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]
        
        
admin.site.register(Order, OrderAdmin)