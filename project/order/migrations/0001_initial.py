# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Order'
        db.create_table('orders', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cart_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('fio_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('city_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('tel_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('dop_tel_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('sms_ot', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('delivery', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('fio_p', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('tel_p', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('dop_tel_p', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('address_p', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('ask_address', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comment', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pred_zvon', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('anon', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('call_after_delivery', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('in_hands', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('make_photo', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('pay_method', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('postcard_text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('postcard_sign', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('customer_notification', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('cart', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('order', ['Order'])


    def backwards(self, orm):
        # Deleting model 'Order'
        db.delete_table('orders')


    models = {
        'order.order': {
            'Meta': {'ordering': "['date_added']", 'object_name': 'Order', 'db_table': "'orders'"},
            'address_p': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'anon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ask_address': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'call_after_delivery': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cart': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'cart_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'city_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'customer_notification': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'delivery': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'dop_tel_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'dop_tel_p': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fio_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fio_p': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_hands': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'make_photo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pay_method': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'postcard_sign': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'postcard_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'pred_zvon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sms_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tel_ot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tel_p': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        }
    }

    complete_apps = ['order']