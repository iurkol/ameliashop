#!-*-coding:utf-8-*-
"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'project.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name

#from catalog.models import Category, Product
import catalog, contman, news, services

class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        
        # append a group for "Administration" & "Applications"
        #self.children.append(modules.Group(
        #    _('Group: Administration & Applications'),
        #    column=1,
        #    collapsible=True,
        #    children = [
        #        modules.AppList(
        #            _('Administration'),
        #            column=1,
        #            collapsible=False,
        #            models=('django.contrib.*',),
        #        ),
        #        modules.AppList(
        #            _('Applications'),
        #            column=1,
        #            css_classes=('collapse closed',),
        #            exclude=('django.contrib.*',),
        #        )
        #    ]
        #))
        
        # append an app list module for "Applications"
        #self.children.append(modules.AppList(
        #    _(u'Приложения сайта'),
        #    collapsible=True,
        #    column=1,
        #    css_classes=('collapse closed',),
        #    exclude=('django.contrib.*',),
        #))
        
        
        #self.children.append(modules.Group(
        #    title=u"Приложения сайта",
        #    column=1,
        #    collapsible=True,
        #    children=[
        #        modules.AppList(
        #            title='Каталог',
        #            models=('catalog.models.*',)
        #        ),
        #        
        #        modules.AppList(
        #            title='Управление контентом страниц',
        #            models=('contman.models.*',)
        #        ),
        #        
        #    ]
        #))
        
        
        self.children.append(modules.ModelList(
            _(u'Каталог'),
            column=1,
            collapsible=True,
            models=('catalog.models.*',),
        ))
        
        self.children.append(modules.ModelList(
            _(u'Изменение контента страниц'),
            column=1,
            collapsible=True,
            models=('contman.models.*',),
        ))
        
        self.children.append(modules.ModelList(
            _(u'Новости'),
            column=1,
            collapsible=True,
            models=('news.models.*',),
        ))
        
        self.children.append(modules.ModelList(
            _(u'Услуги'),
            column=1,
            collapsible=True,
            models=('services.models.*',),
        ))
        
        self.children.append(modules.ModelList(
            _(u'Звонки'),
            column=1,
            collapsible=True,
            models=('calls.models.*',),
        ))
        
        self.children.append(modules.ModelList(
            _(u'Корзина'),
            column=1,
            collapsible=True,
            models=('cart.models.*',),
        ))
        
        self.children.append(modules.ModelList(
            _(u'Заказы'),
            column=1,
            collapsible=True,
            models=('order.models.*',),
        ))
        
        
        
        
        
        # append an app list module for "Administration"
        self.children.append(modules.ModelList(
            _(u'Пользователи и доступ'),
            column=1,
            collapsible=False,
            models=('django.contrib.*',),
        ))
        
        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('Media Management'),
            column=2,
            children=[
                {
                    'title': _('FileBrowser'),
                    'url': '/admin/filebrowser/browse/',
                    'external': False,
                },
            ]
        ))
        
        # append another link list module for "support".
        #self.children.append(modules.LinkList(
        #    _('Support'),
        #    column=2,
        #    children=[
        #        {
        #            'title': _('Django Documentation'),
        #            'url': 'http://docs.djangoproject.com/',
        #            'external': True,
        #        },
        #        {
        #            'title': _('Grappelli Documentation'),
        #            'url': 'http://packages.python.org/django-grappelli/',
        #            'external': True,
        #        },
        #        {
        #            'title': _('Grappelli Google-Code'),
        #            'url': 'http://code.google.com/p/django-grappelli/',
        #            'external': True,
        #        },
        #    ]
        #))
        
        # append a feed module
        #self.children.append(modules.Feed(
        #    _('Latest Django News'),
        #    column=2,
        #    feed_url='http://www.djangoproject.com/rss/weblog/',
        #    limit=5
        #))
        
        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))


