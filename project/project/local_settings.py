#!/usr/bin/env python
#! -*- coding:utf8 -*-

import os, sys

ROOT_PATH = os.path.abspath(__file__)
sys.path.append(ROOT_PATH)



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'ameliadb',                      # Or path to database file if using sqlite3.
        'USER': 'ameliauser',                      # Not used with sqlite3.
        'PASSWORD': 'ameliapass',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

CACHE_BACKEND = 'db://thumbnail_kv_store'

TIME_ZONE = None

LANGUAGE_CODE = 'ru-Ru'
#LANGUAGE_CODE = 'en-En'

MEDIA_ROOT = '/home/iurii/amelia/project/media/'

MEDIA_URL = '/media/'

STATIC_ROOT = '/home/iurii/amelia/project/static/'

STATIC_URL = '/static/'

ADMIN_MEDIA_PREFIX = '/home/iurii/amelia/project/static/admin/'

#TEMPLATE_CONTEXT_PROCESSORS = (
#    'django.core.context_processors.auth',
#    'django.core.context_processors.debug',
#    'django.core.context_processors.i18n',
#    'django.core.context_processors.media',
#    'django.core.context_processors.request',
#    
#    !!!!! Tree of feincms in admin!!!!!!
#    "django.contrib.auth.context_processors.auth",
#    'django.core.context_processors.static'
#)


TEMPLATE_DIRS = ('templates',)

INSTALLED_APPS = (
    'feincms',
    'filebrowser',
    'grappelli.dashboard',
    'grappelli',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    
    # Third party modules
    'south',
    'sorl.thumbnail',
    'mptt',
    'pagination',
    
    # Own modules
    'catalog',
    'contman',
    'news',
    'services',
    'order',
    'calls',
    
)
GRAPPELLI_INDEX_DASHBOARD = 'project.dashboard.CustomIndexDashboard'
GRAPPELLI_ADMIN_TITLE = 'Ameliashop.ru'
paginator_value = 6

#-----------------------------------

SESSION_SAVE_EVERY_REQUEST = True