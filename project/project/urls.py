#!/usr/bin/env python
#! -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from filebrowser.sites import site
import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    
    url(r'^test/', 'catalog.views.test_view'),
    
    url(r'^$', 'catalog.views.index_view'),
    url(r'^cart/index.html/$', 'catalog.views.clear_cart_view'),
    url(r'^about/$', 'catalog.views.about_view'),
    url(r'^delivery/$', 'catalog.views.delivery_view'),
    url(r'^payment/$', 'catalog.views.payment_view'),
    url(r'^discounts/$', 'catalog.views.discounts_view'),
    url(r'^contacts/$', 'catalog.views.contacts_view'),
    url(r'^all_recommendations/$', 'catalog.views.all_recommendations_view'),
    url(r'^all_new_prod/$', 'catalog.views.all_new_prod_view'),
    # Call order form
    url(r'^call/$', 'catalog.views.call_view'),
    # Registration form
    url(r'^register/$', 'catalog.views.register_view'),
    # Login form
    url(r'^login/$', 'catalog.views.login_view'),
    # Logout
    url(r'^logout/$', 'catalog.views.logout_view'),
    # Добавление единицы товара в корзину
    url(r'^add_to_cart/(?P<product_slug>[-\w]+)/$', 'catalog.views.add_to_cart'),
    # Обновляем содержимое и стоимость корзины
    url(r'^ajax_cart_info/$', 'catalog.views.ajax_cart_info_view'),
    # Просмотр категории
    url(r'^category/(?P<category_slug>[-\w]+)/$', 'catalog.views.show_category'),
    # Обнуляем корзину
    url(r'^clear_cart/$', 'catalog.views.clear_cart_view'),
    # Обновляем корзину
    url(r'^updatecart/$', 'catalog.views.updatecart_view'),
    #Форма сортировки
    url(r'^sorting/$', 'catalog.views.sorting_view'),
    
    url(r'^order/$', 'catalog.views.order_view'),
    url(r'^secondstep/$', 'catalog.views.secondstep_view'),
    url(r'^thirdstep/$', 'catalog.views.thirdstep_view'),
    url(r'^fourthstep/$', 'catalog.views.fourthstep_view'),
    url(r'^fifthstep/$', 'catalog.views.fifthstep_view'),
    url(r'^sixthstep/$', 'catalog.views.sixthstep_view'),
    
    url(r'^product/(?P<product_slug>[-\w]+)/$', 'catalog.views.show_product'),
    url(r'^news/(?P<new_slug>[-\w]+)/$', 'catalog.views.show_new'),
    url(r'^service/(?P<service_slug>[-\w]+)/$', 'catalog.views.show_service'),
    url(r'^sims/(?P<product_slug>[-\w]+)/$', 'catalog.views.show_sims_view'),
    url(r'^cart/$', 'catalog.views.show_cart_view'),
    
    url(r'processor/$', 'catalog.views.processor_view'),
    url(r'success/$', 'catalog.views.success_view'),
    url(r'error/$', 'catalog.views.error_view'),                   
                       
                       
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
        }),
    
    
    (r'^grappelli/', include('grappelli.urls')),    
    url(r'^admin/filebrowser/', include(site.urls)),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

