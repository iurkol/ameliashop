#!-*- coding:utf-8 -*-

from django.db import models
    
class Services(models.Model):
    #date = models.CharField(u'Дата', max_length = 18)
    title = models.CharField(u'Название', max_length = 158)
    #anounce = models.TextField(u'Анонс новости на главной странице')
    text = models.TextField(u'Содержимое страницы услуги')
    public = models.BooleanField(verbose_name = u'Опубликовать')
    slug = models.SlugField(unique = True)
    date_add = models.DateTimeField(auto_now_add = True)
    
    class Meta:
        db_table = 'services'
        verbose_name = u'Услуга'
        verbose_name_plural = u'Услуги'
        ordering = ['date_add']
        
    
    def __unicode__(self):
        return self.title
    
    def get_absolute_url(self):
        return self.slug
