$(document).ready(function() {
	$('.popup_link').fancybox({
		scrolling : 'none',
		width : 'auto',
		padding: 10,
//		onClosed: function() {
//    parent.location.reload(true);
//  }
	});
	
	// ñåëåêòû
	var params = {
		changedEl: "select"
	}
	cuSel(params);
	//ãàëëåðåÿ
	$('.fancybox').fancybox({
		scrolling : 'none',
		width : 'auto',
		padding: 20
		
	});
	
	
	
	// Ëèñòàëêà áàííåð
	$('.slide_banner_prev').click(function(){
		swipeL();
	});
	$('.slide_banner_next').click(function(){
		swipeR();
	});
	// Ëèñòàëêà èçîáðàæåíèé òîâàðà
	$('.lit_prev').click(function(){
		var id = $(this).attr('id');
		$(this).addClass('curr').siblings('.lit_prev').removeClass('curr');
		$('#'+id+'_').removeClass('hidden').siblings('.fancy_img').addClass('hidden');
		return false;
	});
	// Ëèñòàëêà êàòàëîã
	$('.slide_next').click(function(){
		var slideWrap = $(this).parents('.list_block').find('.list_');
		var nextLink = $('.slide_next');
		var prevLink = $('.slide_prev');
		var slideWidth = $('.list_item').outerWidth();
		var newLeftPos = slideWrap.position().left - slideWidth;
		if( nextLink.attr('name') == 'next' ) {
			nextLink.removeAttr('name');
			slideWrap.animate({left: newLeftPos}, 500, function(){
				slideWrap
					.find('.list_item:first')
					.appendTo(slideWrap)
					.parent()
					.css({'left': 0});
			});
			setTimeout(function(){ nextLink.attr('name','next') }, 600);
		}
	});
	$('.slide_prev').click(function(){
		var slideWrap = $(this).parents('.list_block').find('.list_');
		var nextLink = $('.slide_next');
		var prevLink = $('.slide_prev');
		var slideWidth = $('.list_item').outerWidth();
		var newLeftPos = slideWrap.position().left - slideWidth;
		if( prevLink.attr('name') == 'prev' ) {
			prevLink.removeAttr('name');
			slideWrap
				.css({'left': newLeftPos})
				.find('.list_item:last')
				.prependTo(slideWrap)
				.parent()
				.animate({left: 0}, 500);
			setTimeout(function(){ prevLink.attr('name','prev') }, 600);
		}
	});
	// Çàêðûâàëêà notify
	$('.notify_block .close').click(function(){
		$('.notify_block').hide(300);
	});
	// èíïóòû è òåêñòàðåà
	$('.inputs_block input').blur(function(){
		var text = $(this).attr('name');
		if ($(this).val()==''){
			$(this).val(text).css('color','#B3B3B3');
		}
	});
	$('.inputs_block input').focus(function(){
		var text = $(this).attr('name');
		if ($(this).val()==text){
			$(this).val('').css('color','#000000');
		}
	});
	$('textarea').blur(function(){
		var text = $(this).attr('name');
		if ($(this).val()==''){
			$(this).val(text).css('color','#B3B3B3');
		}
	});
	$('textarea').focus(function(){
		var text = $(this).attr('name');
		if ($(this).val()==text){
			$(this).val('').css('color','#000000');
		}
	});
	// Êîðçèíà
	CartNum();
	$('#cart_table tr').last().addClass('tr_last');
	$('#cart_table .close').live('click',function(){
		CartDelete($(this).parents('tr') );
	});
	$('.submit.add_to_cart').click(function(){
		$(this).addClass('hidden').siblings('.submit').removeClass('hidden');
	});
	
	$('.password_flds_link').live('click',function(){
		$(this).toggleClass('hidden').siblings('.password_flds_link').toggleClass('hidden');
		$('.password_fld').toggleClass('hidden');
		
		return false;
	});
	$('.password_fld').keyup(function() {
		var val = $(this).val();
		$(this).siblings().val(val);
	});
	
	// ×åêáîêñû è ðàäèîáàòòîíû
	$('.checkbox div').click(function() {
		var parent = $(this).parents('.checkbox').first();
		if( parent.hasClass('checked') ) {
			parent.removeClass('checked');
			parent.find('input').removeAttr('checked');
		}
		else {
			parent.addClass('checked');
			parent.find('input').attr('checked','checked');
		}
	});
	$('label').click(function() {
		var id = $(this).attr('id');
		var parent = $(this).siblings('.checkbox').first();
		if( parent.hasClass('checked') ) {
			parent.removeClass('checked');
			parent.find('input').removeAttr('checked');
		}
		else {
			parent.addClass('checked');
			parent.find('input').attr('checked','checked');
		}
	});
	$('form').change(function(){
		var id = $(this).find('.radio .appearance:checked').attr('id');
		var parent = $('#'+id).parents('.radio');
		parent.addClass('checked');
		parent.find('input.appearance').attr('checked','checked');
		
		$('#'+id+'_').removeClass('hidden').siblings('.btn_if').addClass('hidden');
		$('#'+id+'__').removeClass('hidden').siblings('.form_table').addClass('hidden');
		
		$(this).find('.appearance').not('#'+id).parents('.radio').each(function(){
			$(this).removeClass('checked');
			$(this).find('input.appearance').removeAttr('checked');
		});
	});
	$('form').change(function(){
		var id = $(this).find('.radio .inside_radio:checked').attr('id');
		var parent = $('#'+id).parents('.radio');
		parent.addClass('checked');
		parent.find('input.inside_radio').attr('checked','checked');
		
		$(this).find('.inside_radio').not('#'+id).parents('.radio').each(function(){
			$(this).removeClass('checked');
			$(this).find('input.inside_radio').removeAttr('checked');
		});
	});
	$('.radio div').click(function(){
		if ($(this).siblings('input').hasClass('inside_radio')) {
			var id = $(this).siblings('.inside_radio').attr('id');
			var parent = $('#'+id).parents('.radio');
			parent.addClass('checked');
			parent.find('input.inside_radio').attr('checked','checked');
			
			$(this).parents('form').find('.inside_radio').not('#'+id).parents('.radio').each(function(){
				$(this).removeClass('checked');
				$(this).find('input.inside_radio').removeAttr('checked');
			});
		}
		else {
			var id = $(this).siblings('.appearance').attr('id');
			var parent = $('#'+id).parents('.radio');
			parent.addClass('checked');
			parent.find('input.appearance').attr('checked','checked');
		
			$('#'+id+'_').removeClass('hidden').siblings('.btn_if').addClass('hidden');
			$('#'+id+'__').removeClass('hidden').siblings('.form_table').addClass('hidden');
			
			$(this).parents('form').find('.appearance').not('#'+id).parents('.radio').each(function(){
				$(this).removeClass('checked');
				$(this).find('input.appearance').removeAttr('checked');
			});
		}
	});
});
$(document).click(function(e){
	
});
$(window).scroll(function(){
	
});

// Ïåðåêëþ÷àëêà áàííåð
function swipeL() {
	var img = $('.slide_banner_list').find('.slide_banner_item');
	var curr_img = null;
	img.each(function(){
		if( !$(this).hasClass('hidden') ){
			curr_img = $(this)
		}
	});
	var id = curr_img.attr('id');
	var arr_id = id.split('_');
	var curr_id = arr_id[0];
	var curr_num = arr_id[1]*1;
	var next_num = curr_num-1;
	if(next_num<1  ) {
		next_num = img.length;
	}
	$('#'+curr_id+'_'+next_num+'_').removeClass('hidden').siblings('.slide_banner_item').addClass('hidden');
	//$('#'+curr_id+'_'+next_num).addClass('curr').siblings('.switch').removeClass('curr');
};
function swipeR() {
	var img = $('.slide_banner_list').find('.slide_banner_item');
	var curr_img = null;
	img.each(function(){
		if( !$(this).hasClass('hidden') ){
			curr_img = $(this)
		}
	});
	var id = curr_img.attr('id');
	var arr_id = id.split('_');
	var curr_id = arr_id[0];
	var curr_num = arr_id[1]*1;
	var next_num = curr_num+1;
	if( $('#'+curr_id+'_'+next_num+'_').length<1 ) {
		next_num = 1;
	}
	$('#'+curr_id+'_'+next_num+'_').removeClass('hidden').siblings('.slide_banner_item').addClass('hidden');
	//$('#'+curr_id+'_'+next_num).addClass('curr').siblings('.switch').removeClass('curr');
};

// Ïåðåêëþ÷àëêà èçîáðàæåíèé òîâàðà
function itemL() {
	var img = $('.round_item').find('.fancy_img');
	var curr_img = null;
	img.each(function(){
		if( !$(this).hasClass('hidden') ){
			curr_img = $(this)
		}
	});
	var id = curr_img.attr('id');
	var arr_id = id.split('_');
	var curr_id = arr_id[0];
	var curr_num = arr_id[1]*1;
	var next_num = curr_num-1;
	if(next_num<1  ) {
		next_num = img.length;
	}
	$('#'+curr_id+'_'+next_num+'_').removeClass('hidden').siblings('.fancy_img').addClass('hidden');
	$('#'+curr_id+'_'+next_num).addClass('curr').siblings('.lit_prev').removeClass('curr');
};
function itemR() {
	var img = $('.slide_banner_list').find('.fancy_img');
	var curr_img = null;
	img.each(function(){
		if( !$(this).hasClass('hidden') ){
			curr_img = $(this)
		}
	});
	var id = curr_img.attr('id');
	var arr_id = id.split('_');
	var curr_id = arr_id[0];
	var curr_num = arr_id[1]*1;
	var next_num = curr_num+1;
	if( $('#'+curr_id+'_'+next_num+'_').length<1 ) {
		next_num = 1;
	}
	$('#'+curr_id+'_'+next_num+'_').removeClass('hidden').siblings('.fancy_img').addClass('hidden');
	$('#'+curr_id+'_'+next_num).addClass('curr').siblings('.lit_prev').removeClass('curr');
};


// Ñ÷èòàëêà êîðçèíû
function CartNum() {
	$('.plus').click(function(){
		var iVal = parseInt($(this).siblings('.cart_action_num').html() );
		iVal++;
		$(this).siblings('.cart_action_num').html(iVal);
		
		CartTotalPrice();
	});	
	$('.minus').click(function(){
		var iVal = parseInt($(this).siblings('.cart_action_num').html() );
		iVal--;
		
		if(iVal<1) {
			CartDelete($(this).parents('tr'));
		}
		else {
			$(this).siblings('.cart_action_num').html(iVal);
		}
		CartTotalPrice();
	});
}
function CartTotalPrice() {
	var iTotal = 0;
	$('#cart_table tr').each(function(){
		
		if( $(this).find('.cart_action_num').length>0 && $(this).find('.price_num').length>0 ) {
			var iVal = parseInt($(this).find('.cart_action_num').html() );
			var iPrice = parseInt($(this).find('.price_num').html().replace(' ','') );
			iTotal += iVal*iPrice;
		}
	});
	
	if(iTotal>0) {
		var aPrice = iTotal.toString().split('');
		var iL = aPrice.length;
		var iCount = 1;
		for(var i=iL-1; i>-0; i--) {
			var sSep = (iCount%3 === 0) ? ' ' : '';
			aPrice[i] = sSep+aPrice[i];
			iCount++;
		}
		$('.price_total').html( aPrice.join('') );
	}
	else {
		location.href = 'index.html';
	}
}
function CartDelete( oRow ) {
	oRow.css('overflow','hidden');
	oRow.find('td').css({'overflow':'hidden'});
	
	oRow.animate({'opacity': 0},300,function(){
		oRow.find('td').css({'padding':0});
		oRow.animate({'height': 0},300);
		oRow.find('*').animate({'height': 0},300,function(){
			oRow.remove();
			CartTotalPrice();
			$('#cart_table tr').last().addClass('tr_last');
		});
	});
}







