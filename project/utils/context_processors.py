#!/usr/bin/env python
#!-*-coding: utf-8-*-
from catalog.models import Category, Flower

def get_categories_list(request):
    '''This function returns queryset of all category objects'''
    cat_list = Category.objects.all()
    return {
        'cats' : cat_list
    }